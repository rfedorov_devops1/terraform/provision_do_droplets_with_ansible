variable "digitalocean_token" {
  description = "Access token to the DigitalOcean API"
  type        = string
}

variable "droplet_instance_region" {
  description = "Droplet instance region"
  type        = string
}

variable "droplet_instance_image" {
  description = "Droplet instance image"
  type        = string
}

variable "droplet_instance_size" {
  description = "Droplet instance size"
  type        = string
}

variable "killemall1_key_name" {
  description = "killemall1 key name in cloud"
  type        = string
}

variable "path_to_pubkey_killemall1" {
  description = "Public key path"
  type        = string
}

variable "path_to_privatekey_killemall1" {
  description = "Private key path"
  type        = string
}

variable "aws_access_key" {
  description = "AWS Access Key"
  type        = string
}

variable "aws_secret_key" {
  description = "AWS Secret Key"
  type        = string
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
}

variable "aws_route_53_zone_name" {
  description = "AWS Route53 zone name"
  type        = string
}

variable "root_password" {
  description = "Password for root user"
  type        = string
}

variable "devops_tag" {
  description = "Tag devops"
  type        = string
}

variable "email_tag" {
  description = "Tag email"
  type        = string
}

variable "password_length" {
  description = "Password length"
  type        = number
}

variable "use_special_characters_in_password" {
  description = "Use special characters in the password"
  type        = bool
}

variable "use_numbers_in_password" {
  description = "Use numbers in the password"
  type        = bool
}

variable "use_upper_symbols_in_password" {
  description = "Use upper symbols in the password"
  type        = bool
}

variable "use_lower_symbols_in_password" {
  description = "Use lower symbols in the password"
  type        = bool
}

variable "site_droplet_count" {
  description = "Number of droplets site"
  type        = number
}

variable "lb_droplet_count" {
  description = "Number of droplets lb"
  type        = number
}

variable "result_file" {
  description = "Output the execution result to a file"
  type        = string
}

variable "ansible_inventory" {
  description = "The file where ansible inventory will be saved"
  type        = string
}
