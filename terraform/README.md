## Usage example
> After moving the terraform.tfvars file, fill in the values of the variables inside it

    $ сd ansible/terraform
    $ mv terraform.tfvars.simple terraform.tfvars
    $ terraform init
    $ terraform fmt
    $ terraform validate
    $ terraform plan -out planfile
    $ terraform apply planfile

## Terraform documentation generator
[cytopia/docker-terraform-docs](https://github.com/cytopia/docker-terraform-docs)

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 3.53.0 |
| <a name="requirement_digitalocean"></a> [digitalocean](#requirement\_digitalocean) | 2.10.1 |
| <a name="requirement_local"></a> [local](#requirement\_local) | 2.1.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | 3.1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.53.0 |
| <a name="provider_digitalocean"></a> [digitalocean](#provider\_digitalocean) | 2.10.1 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.1.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route53_record.lb_fqdn](https://registry.terraform.io/providers/hashicorp/aws/3.53.0/docs/resources/route53_record) | resource |
| [aws_route53_record.site_fqdn](https://registry.terraform.io/providers/hashicorp/aws/3.53.0/docs/resources/route53_record) | resource |
| [digitalocean_droplet.lb](https://registry.terraform.io/providers/digitalocean/digitalocean/2.10.1/docs/resources/droplet) | resource |
| [digitalocean_droplet.site](https://registry.terraform.io/providers/digitalocean/digitalocean/2.10.1/docs/resources/droplet) | resource |
| [digitalocean_ssh_key.push_killemall1_pubkey](https://registry.terraform.io/providers/digitalocean/digitalocean/2.10.1/docs/resources/ssh_key) | resource |
| [digitalocean_tag.push_devops_tag](https://registry.terraform.io/providers/digitalocean/digitalocean/2.10.1/docs/resources/tag) | resource |
| [digitalocean_tag.push_email_tag](https://registry.terraform.io/providers/digitalocean/digitalocean/2.10.1/docs/resources/tag) | resource |
| [local_file.build_ansible_inventory](https://registry.terraform.io/providers/hashicorp/local/2.1.0/docs/resources/file) | resource |
| [local_file.output_result_to_file](https://registry.terraform.io/providers/hashicorp/local/2.1.0/docs/resources/file) | resource |
| [random_password.password_lb](https://registry.terraform.io/providers/hashicorp/random/3.1.0/docs/resources/password) | resource |
| [random_password.password_site](https://registry.terraform.io/providers/hashicorp/random/3.1.0/docs/resources/password) | resource |
| [aws_route53_zone.get_devops_zone_id](https://registry.terraform.io/providers/hashicorp/aws/3.53.0/docs/data-sources/route53_zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ansible_inventory"></a> [ansible\_inventory](#input\_ansible\_inventory) | The file where ansible inventory will be saved | `string` | n/a | yes |
| <a name="input_aws_access_key"></a> [aws\_access\_key](#input\_aws\_access\_key) | AWS Access Key | `string` | n/a | yes |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region | `string` | n/a | yes |
| <a name="input_aws_route_53_zone_name"></a> [aws\_route\_53\_zone\_name](#input\_aws\_route\_53\_zone\_name) | AWS Route53 zone name | `string` | n/a | yes |
| <a name="input_aws_secret_key"></a> [aws\_secret\_key](#input\_aws\_secret\_key) | AWS Secret Key | `string` | n/a | yes |
| <a name="input_devops_tag"></a> [devops\_tag](#input\_devops\_tag) | Tag devops | `string` | n/a | yes |
| <a name="input_digitalocean_token"></a> [digitalocean\_token](#input\_digitalocean\_token) | Access token to the DigitalOcean API | `string` | n/a | yes |
| <a name="input_droplet_instance_image"></a> [droplet\_instance\_image](#input\_droplet\_instance\_image) | Droplet instance image | `string` | n/a | yes |
| <a name="input_droplet_instance_region"></a> [droplet\_instance\_region](#input\_droplet\_instance\_region) | Droplet instance region | `string` | n/a | yes |
| <a name="input_droplet_instance_size"></a> [droplet\_instance\_size](#input\_droplet\_instance\_size) | Droplet instance size | `string` | n/a | yes |
| <a name="input_email_tag"></a> [email\_tag](#input\_email\_tag) | Tag email | `string` | n/a | yes |
| <a name="input_killemall1_key_name"></a> [killemall1\_key\_name](#input\_killemall1\_key\_name) | killemall1 key name in cloud | `string` | n/a | yes |
| <a name="input_lb_droplet_count"></a> [lb\_droplet\_count](#input\_lb\_droplet\_count) | Number of droplets lb | `number` | n/a | yes |
| <a name="input_password_length"></a> [password\_length](#input\_password\_length) | Password length | `number` | n/a | yes |
| <a name="input_path_to_privatekey_killemall1"></a> [path\_to\_privatekey\_killemall1](#input\_path\_to\_privatekey\_killemall1) | Private key path | `string` | n/a | yes |
| <a name="input_path_to_pubkey_killemall1"></a> [path\_to\_pubkey\_killemall1](#input\_path\_to\_pubkey\_killemall1) | Public key path | `string` | n/a | yes |
| <a name="input_result_file"></a> [result\_file](#input\_result\_file) | Output the execution result to a file | `string` | n/a | yes |
| <a name="input_root_password"></a> [root\_password](#input\_root\_password) | Password for root user | `string` | n/a | yes |
| <a name="input_site_droplet_count"></a> [site\_droplet\_count](#input\_site\_droplet\_count) | Number of droplets site | `number` | n/a | yes |
| <a name="input_use_lower_symbols_in_password"></a> [use\_lower\_symbols\_in\_password](#input\_use\_lower\_symbols\_in\_password) | Use lower symbols in the password | `bool` | n/a | yes |
| <a name="input_use_numbers_in_password"></a> [use\_numbers\_in\_password](#input\_use\_numbers\_in\_password) | Use numbers in the password | `bool` | n/a | yes |
| <a name="input_use_special_characters_in_password"></a> [use\_special\_characters\_in\_password](#input\_use\_special\_characters\_in\_password) | Use special characters in the password | `bool` | n/a | yes |
| <a name="input_use_upper_symbols_in_password"></a> [use\_upper\_symbols\_in\_password](#input\_use\_upper\_symbols\_in\_password) | Use upper symbols in the password | `bool` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_droplets_describe_lb"></a> [droplets\_describe\_lb](#output\_droplets\_describe\_lb) | n/a |
| <a name="output_droplets_describe_site"></a> [droplets\_describe\_site](#output\_droplets\_describe\_site) | n/a |
