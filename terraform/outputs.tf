output "droplets_describe_lb" {
  value = [
    for index, dict_index in digitalocean_droplet.lb : {
      droplet_name       = dict_index.name
      droplet_size       = dict_index.size
      droplet_ip_address = digitalocean_droplet.lb[index].ipv4_address
      droplet_fqdn       = aws_route53_record.lb_fqdn[index].fqdn
      root_password      = nonsensitive(random_password.password_lb[index].result)
    }
  ]
}

output "droplets_describe_site" {
  value = [
    for index, dict_index in digitalocean_droplet.site : {
      droplet_name       = dict_index.name
      droplet_size       = dict_index.size
      droplet_ip_address = digitalocean_droplet.site[index].ipv4_address
      droplet_fqdn       = aws_route53_record.site_fqdn[index].fqdn
      root_password      = nonsensitive(random_password.password_site[index].result)
    }
  ]
}
