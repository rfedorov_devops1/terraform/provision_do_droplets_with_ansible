terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.10.1"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "3.53.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.1.0"
    }
  }
}

# Settings for the DigitalOcean provider
provider "digitalocean" {
  token = var.digitalocean_token
}

# Settings for the random provider
provider "random" {}

# Settings for the AWS provider
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

# Creating an devops tag
resource "digitalocean_tag" "push_devops_tag" {
  name = var.devops_tag
}

# Creating an email tag
resource "digitalocean_tag" "push_email_tag" {
  name = var.email_tag
}

# Adding a public key
resource "digitalocean_ssh_key" "push_killemall1_pubkey" {
  name       = var.killemall1_key_name
  public_key = file(var.path_to_pubkey_killemall1)
}

# Search for the devops zone ID
data "aws_route53_zone" "get_devops_zone_id" {
  name = "${var.aws_route_53_zone_name}."
}

# Random password generator for lb
resource "random_password" "password_lb" {
  count  = var.lb_droplet_count
  length   = var.password_length
  special  = var.use_special_characters_in_password
  number   = var.use_numbers_in_password
  upper    = var.use_upper_symbols_in_password
  lower    = var.use_lower_symbols_in_password
}

# Creating a droplet for lb
resource "digitalocean_droplet" "lb" {
  count  = var.lb_droplet_count
  image  = "ubuntu-20-04-x64"
  name   = "lb-${count.index}"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [
    digitalocean_ssh_key.push_killemall1_pubkey.id
  ]
  tags = [
    digitalocean_tag.push_devops_tag.name,
    digitalocean_tag.push_email_tag.name
  ]
  provisioner "remote-exec" {
    inline = [
      "echo 'root:${nonsensitive(random_password.password_lb[count.index].result)}' | chpasswd"
    ]
    connection {
      type        = "ssh"
      user        = "root"
      private_key = file(var.path_to_privatekey_killemall1)
      host        = self.ipv4_address
    }
  }
}

# Adding an FQDN for a droplet lb
resource "aws_route53_record" "lb_fqdn" {
  count   = var.lb_droplet_count
  zone_id = data.aws_route53_zone.get_devops_zone_id.zone_id
  name    = "lb-${count.index}"
  type    = "A"
  ttl     = "10"
  records = ["${element(digitalocean_droplet.lb.*.ipv4_address, count.index)}"]
}

# Random password generator for site
resource "random_password" "password_site" {
  count  = var.site_droplet_count
  length   = var.password_length
  special  = var.use_special_characters_in_password
  number   = var.use_numbers_in_password
  upper    = var.use_upper_symbols_in_password
  lower    = var.use_lower_symbols_in_password
}

# Creating a droplet for site
resource "digitalocean_droplet" "site" {
  count  = var.site_droplet_count
  image  = "ubuntu-20-04-x64"
  name   = "site-${count.index}"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [
    digitalocean_ssh_key.push_killemall1_pubkey.id
  ]
  tags = [
    digitalocean_tag.push_devops_tag.name,
    digitalocean_tag.push_email_tag.name
  ]
  provisioner "remote-exec" {
    inline = [
      "echo 'root:${nonsensitive(random_password.password_site[count.index].result)}' | chpasswd"
    ]
    connection {
      type        = "ssh"
      user        = "root"
      private_key = file(var.path_to_privatekey_killemall1)
      host        = self.ipv4_address
    }
  }
}

# Adding an FQDN for a droplet site
resource "aws_route53_record" "site_fqdn" {
  count   = var.site_droplet_count
  zone_id = data.aws_route53_zone.get_devops_zone_id.zone_id
  name    = "site-${count.index}"
  type    = "A"
  ttl     = "10"
  records = ["${element(digitalocean_droplet.site.*.ipv4_address, count.index)}"]
}

# Output the execution result to a file 
resource "local_file" "output_result_to_file" {
  content = <<EOT
%{for index, name in digitalocean_droplet.lb~}
${index + 1};${aws_route53_record.lb_fqdn[index].fqdn};${digitalocean_droplet.lb[index].ipv4_address};${random_password.password_lb[index].result}
%{endfor~}
%{for index, name in digitalocean_droplet.site~}
${index + 1};${aws_route53_record.site_fqdn[index].fqdn};${digitalocean_droplet.site[index].ipv4_address};${random_password.password_site[index].result}
%{endfor~}
EOT
  filename = var.result_file
}

# Output the builded ansible inventory to a file 
resource "local_file" "build_ansible_inventory" {
  content  = <<EOT
---
all:
  children:
    lb:
      hosts:
%{for index,name in digitalocean_droplet.lb~}
        ${digitalocean_droplet.lb[index].name}:
          ansible_host: ${digitalocean_droplet.lb[index].ipv4_address}
          host_fqdn: ${aws_route53_record.lb_fqdn[index].fqdn}
          ansible_ssh_private_key_file: ../terraform/${var.path_to_privatekey_killemall1}
%{endfor~}
      vars:
        ansible_user: root
    site:
      hosts:
%{for index,name in digitalocean_droplet.site~}
        ${digitalocean_droplet.site[index].name}:
          ansible_host: ${digitalocean_droplet.site[index].ipv4_address}
          host_fqdn: ${aws_route53_record.site_fqdn[index].fqdn}
          ansible_ssh_private_key_file: ../terraform/${var.path_to_privatekey_killemall1}
%{endfor~}
    killemall:
      children:
        site:
        lb:
      vars:
        ansible_user: root
...

EOT
  filename = var.ansible_inventory
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory.yml -e '{\"servers\": \"killemall\", \"serial_count\": 0}' nginx.yml --skip-tags nginx_uninstall"
    working_dir = "../ansible"
  }
}
