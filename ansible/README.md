<div align ="center"><b>Ansible: nginx install, configure or uninstall</b></div>

[[_TOC_]]

## Description
nginx.yml playbook allows you to install, configure vhosts or remove nginx

## System requirements
- Ubuntu 20.04 or CentOS 7
- Ansible 2.11.3 and higher
    - ![community.general.snap](https://docs.ansible.com/ansible/latest/collections/community/general/snap_module.html) collection is installed

## Description of tags
#### uninstall
- Removing nginx if variables **nginx_uninstall_accept** equal "true" and / or certbot if variables **letsencrypt_uninstall_accept** equal "true"
    - removing nginx with conf files (see variable **nginx_uninstall_accept**)
    - removing certbot with ssl-certificates (see variable **letsencrypt_uninstall_accept**)

## Description of variables
#### servers
* Server group in inventory
* Type: ```String```
* Default value: ```null```

#### serial_count
* The number of hosts on which tasks will run simultaneously
* Type: ```Integer```
* Default value: ```1```

#### nginx_version_ubuntu
* Nginx version for Ubuntu.
* Type: ```String```
* Default value: ```1.21.3-1~focal```

#### nginx_version_centos
* Nginx version for Ubuntu.
* Type: ```String```
* Default value: ```1.21.3```

#### nginx_uninstall_accept
* Removing nginx along with config files. Used in conjunction with the **uninstall** tag
* Type: ```Boolean```
* Default value: ```false```

## Example of usage
#### Running nginx and certbot installation with vhosts config using SSL
    $ ansible-playbook nginx.yml -i inventory.yml -e '{"servers": "killemall"}' --skip-tags "nginx_uninstall"

#### Removing nginx if variables nginx_uninstall_accept equal "true"
    $ ansible-playbook nginx.yml -i inventory.yml -e '{"servers": "killemall"}' --tags "nginx_uninstall"
